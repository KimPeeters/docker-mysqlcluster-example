#!/bin/bash

sed -i 's/^bind-address/#bind-address/g' /etc/mysql/my.cnf
sed -i 's/^socket/#socket/g' /etc/mysql/my.cnf
sed -i "s/^#server-id.*$/server-id=1/g" /etc/mysql/my.cnf
sed -i "s/^#log_bin.*$/log-bin=mysql-bin/g" /etc/mysql/my.cnf

# ckeck if database files exists, if not, copy skeleton
if [ ! -d "/var/lib/mysql/mysql" ]; then
  cp -R /var/lib/mysql-skel/* /var/lib/mysql/
  chown -R mysql.mysql /var/lib/mysql
  chmod 700 /var/lib/mysql
fi

/etc/init.d/mysql start
/etc/init.d/mysql status

if [ "$?" != "0" ];then
  exit $?
fi

mysql -uroot -e "show databases;"
if [ "$?" == "0" ];then
  mysqladmin -u root password rootpass
  mysql -uroot -prootpass -e "GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY 'rootpass' WITH GRANT OPTION;"
  mysql -uroot -prootpass -e "GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%' IDENTIFIED BY 'slavepass';"
fi

tail -f /var/log/mysql.log
