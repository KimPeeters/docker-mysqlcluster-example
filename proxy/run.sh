#!/bin/bash

dec2ip () {
    local ip dec=$@
    for e in {3..0}
    do
        ((octet = dec / (256 ** e) ))
        ((dec -= octet * 256 ** e))
        ip+=$delim$octet
        delim=.
    done
    printf '%s\n' "$ip"
}


sed -i 's/mysql_ifaces="127.0.0.1:6032/mysql_ifaces="0.0.0.0:6032/g' /etc/proxysql.cnf
/etc/init.d/proxysql start
/etc/init.d/proxysql status

if [ "$?" != "0" ];then
  exit $?
fi

sleep 10

# add master
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "INSERT INTO main.mysql_servers (hostgroup_id, hostname, port, max_replication_lag) VALUES (0, 'master', 3306, 20);"

# add slaves
#mysql -uroot -prootpass -hmaster -N -B -e "show slave hosts" | awk '{print $1}' | while read -r data
#do
#  CURRENTMYSQLIP=$(dec2ip $data)
#  mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "INSERT INTO main.mysql_servers (hostgroup_id, hostname, port, max_replication_lag) VALUES (1, '$CURRENTMYSQLIP', 3306, 20);"
#done

# add user
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "INSERT INTO main.mysql_users (username, password, active, default_hostgroup, max_connections) VALUES ('root', 'rootpass', 1, 0, 200);"

# add rules
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "INSERT INTO main.mysql_query_rules (active, match_pattern, destination_hostgroup, cache_ttl) VALUES (1, '^SELECT .* FOR UPDATE', 0, NULL);"
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "INSERT INTO main.mysql_query_rules (active, match_pattern, destination_hostgroup, cache_ttl) VALUES (1, '^SELECT .*', 1, NULL);"


mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "LOAD MYSQL USERS TO RUNTIME;"
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "LOAD MYSQL QUERY RULES TO RUNTIME;"
mysql -uadmin -padmin -h127.0.0.1 -P6032 -e "LOAD MYSQL SERVERS TO RUNTIME;"

tail -f /var/log/faillog



