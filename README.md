# MySQL cluster

MySQL cluster in docker

### Usage
Don't start more than 1 master or proxy at the same time. 

First build the containers `docker-compose build`, then start 1 master `docker-compose scale master=1`. 

Now you can enter this container (with `docker-enter`) and check if the master is running. Default password for root is `rootpass`. 

After this you can start multiple slaves `docker-compose scale slave=5`. This starts 5 slaves. After a few seconds you can enter one of the slave container and verify if it's running with `mysql -uroot -prootpass -e "show slave status"`

When all containers are up and running, start the proxy `docker-compose scale proxy=1`. 

Now you can connect to your server on port 6033 like it's a normal MySQL server. Reads are executed on the slaves, all other queries are executed on the master. To stop everything just run `docker-compose scale proxy=0 slaves=0 master=0`

### Control script
To make it easy, there is a control script.

Run `./ctl start 4` to start the master, proxy, manager and 4 slave.

Run `./ctl` for more info

### Sources
https://github.com/sysown/proxysql

http://stackoverflow.com/questions/10768160/ip-address-converter

http://severalnines.com/blog/how-proxysql-adds-failover-and-query-control-your-mysql-replication-setup
