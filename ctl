#!/bin/bash


case "$1" in
  start)
        docker-compose scale master=1
        sleep 5
        docker-compose scale proxy=1 manager=1
        sleep 2
        re='^[0-9]+$'
        if [[ $2 =~ $re ]] ; then
            docker-compose scale slave=$2
        else
            docker-compose scale slave=5
        fi
        ;;
  stop)
        docker-compose scale slave=0 manager=0 proxy=0 master=0
        ;;
  reload|restart|force-reload)
        NUMSLAVES=$(docker-compose ps | grep slave | wc -l)
        docker-compose scale slave=0 manager=0 proxy=0 master=0
        docker-compose scale master=1
        sleep 5
        docker-compose scale proxy=1 manager=1
        sleep 2
        docker-compose scale slave=$NUMSLAVES
        ;;
  status)
        docker-compose ps
        ;;
  *)
        echo "Usage: $N {start|stop|restart|force-reload|status}" >&2
        exit 1
        ;;
esac

exit 0

