#!/bin/bash

RAWINFO=$(redis-cli blpop mysqlcluster 10 | awk 'NR>1')
if [ "$RAWINFO" == "" ]; then
  exit 0
fi

IFS=' ' read -r -a INFO <<< "$RAWINFO"

case "${INFO[0]}" in
  reload )
    echo RELOAD!
    exit 0
    ;;
  add )
    echo "ADD ${INFO[1]}"
    mysql -uadmin -padmin -hproxy -P6032 -e "INSERT INTO main.mysql_servers (hostgroup_id, hostname, port, max_replication_lag) VALUES (1, '${INFO[1]}', 3306, 20);"
    exit 0;
    ;;
  remove )
    echo "REMOVE ${INFO[1]}"
    mysql -uadmin -padmin -hproxy -P6032 -e "DELETE FROM main.mysql_servers WHERE hostname = '${INFO[1]}';"
    ;;
esac
