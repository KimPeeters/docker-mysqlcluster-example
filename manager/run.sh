#!/bin/bash

sed -i 's/^bind 127\.0\.0\.1$/bind 0.0.0.0/g' /etc/redis/redis.conf

/etc/init.d/redis-server start
/etc/init.d/redis-server status

if [ "$?" != "0" ];then
  exit $?
fi

while true; do
  /usr/local/bin/service.sh
done
