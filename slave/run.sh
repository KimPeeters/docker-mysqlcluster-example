#!/usr/bin/env bash

CURRENTIP=$(hostname -i)

function clean_up {
        # Perform program exit housekeeping
        echo remove from cluster
        redis-cli -h manager rpush mysqlcluster "remove $CURRENTIP"
        exit 0
}

trap clean_up SIGHUP SIGINT SIGTERM
#trap 'exit 0' SIGTERM


ip2dec () {
    local a b c d ip=$@
    IFS=. read -r a b c d <<< "$ip"
    printf '%d\n' "$((a * 256 ** 3 + b * 256 ** 2 + c * 256 + d))"
}

sed -i 's/^bind-address/#bind-address/g' /etc/mysql/my.cnf
sed -i 's/^socket/#socket/g' /etc/mysql/my.cnf
mysqlserverid=`ip2dec $(hostname -i)`
sed -i "s/^#server-id.*$/server-id=$mysqlserverid/g" /etc/mysql/my.cnf

/etc/init.d/mysql start
/etc/init.d/mysql status

if [ "$?" != "0" ];then
  cat /var/log/mysql.err
  exit $?
fi

mysql -uroot -e "show databases;"
if [ "$?" == "0" ];then
  mysqladmin -u root password rootpass
  mysql -uroot -prootpass -e "GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY 'rootpass' WITH GRANT OPTION;"
  if [ "$?" != "0" ];then
    exit $?
  fi

  echo CHANGE MASTER TO
  mysql -uroot -prootpass -h127.0.0.1 -e "CHANGE MASTER TO MASTER_HOST='master',MASTER_USER='repl',MASTER_PASSWORD='slavepass';"
  if [ "$?" != "0" ];then
    exit $?
  fi

  echo COPY DATA
  mysqldump -hmaster -uroot -prootpass --all-databases --master-data --single-transaction | mysql -uroot -prootpass -h127.0.0.1
  if [ "$?" != "0" ];then
    exit $?
  fi

  echo START SLAVE
  mysql -uroot -prootpass -h127.0.0.1 -e "START SLAVE;"
  if [ "$?" != "0" ];then
    exit $?
  fi

fi

# add server to cluster
redis-cli -h manager rpush mysqlcluster "add $CURRENTIP"

while true; do sleep 1; done
#echo TAIL LOG
#tail -f /var/log/mysql.err
